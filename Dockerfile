# pull official base image
FROM node:lts-alpine

# set working directory
WORKDIR /app

# add app
COPY . ./

# install app dependencies
RUN npm install --silent

# start app
CMD ["npm", "start"]