import React, { useState } from "react";
import {
    Anchor,
    Box,
    Button,
    Grommet,
    grommet,
    Header,
    Page,
    PageContent,
    PageHeader,
    ResponsiveContext,
    Text,
} from 'grommet';
import GetOnlinePosts from './components/postlist/GetOnlinePosts';
import { Moon, Sun } from "grommet-icons";
import { deepMerge } from "grommet/utils";
import "./global.js"

/*
color plate: https://colorkit.co/palette/c7522a-e5c185-fbf2c4-74a892-008585/
- #c7522a
- #e5c185
- #fbf2c4
- #74a892
- #008585
*/

const theme = deepMerge(grommet, {
    global: {
        colors: {
            brand: '#c7522a',
            bg: {
                dark: '#74a892',
                light: '#fbf2c4',
            },
            card_bg: {
                dark: '#008585',
                light: '#e5c185',
            },
            text: {
                dark: '#e5c185',
                light: '#008585',
            },
            anchor: '#c7522a',
        },
        font: {
            family: "Roboto",
            size: "18px",
            height: "20px",
        },
    },
});

const AppBar = (props) => (
 <Header
    background="card_bg"
    pad={{ left: "medium", right: "small", vertical: "small" }}
    elevation="medium"
    sticky="scrollup"
    {...props}
 />
);

function App() {

    const [dark, setDark] = useState(false);

    return (
    <Grommet theme={theme} full themeMode={dark ? "dark" : "light"}>
        <Page background={{image: "url('ssspot.svg')", opacity: "strong"}} id={global.top_link}>
            <AppBar>
                <Anchor href={"#"+global.top_link} >
                    <Text size="large" color="brand">FastBlog</Text>
                </Anchor>
                <Button
                    a11yTitle={dark ? "Switch to Light Mode" : "Switch to Dark Mode"}
                    icon={dark ? <Moon /> : <Sun />}
                    onClick={() => setDark(!dark)}
                    tip={{
                        content: (
                            <Box
                                pad="small"
                                round="small"
                                background="bg"
                            >
                                {dark ? "Switch to Light Mode" : "Switch to Dark Mode"}
                            </Box>
                          ),
                          plain: true,
                        }}
                />
            </AppBar>
            <PageContent>
                <PageHeader/>
                <GetOnlinePosts></GetOnlinePosts>
            </PageContent>
        </Page>
    </Grommet>
  );
}

export default App;
