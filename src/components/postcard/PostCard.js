import { Component } from 'react';
import moment from 'moment';
import {
    Anchor,
    Box,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Markdown,
    Heading,
    Text,
} from "grommet";
import {
    LinkTop,
    LinkUp,
} from "grommet-icons";

class PostCard extends Component {

    render() {
        var post = this.props.post

        return(
            <Card id={post.id} background="card_bg">
                <CardHeader pad="medium">
                    <Heading level={2} margin="none">
                        <Anchor color="anchor" href={"#"+post.id}>
                            {post.title}
                        </Anchor>
                    </Heading>
                </CardHeader>
                <CardBody pad="medium">
                    <Markdown>
                        {post.body}
                    </Markdown>
                </CardBody>
                <CardFooter pad="medium" background="background-contrast">
                    <Box direction="row">
                        <Anchor color="anchor" href={"#"+post.id} icon={<LinkUp />}/>
                        <Anchor color="anchor" href={"#"+global.top_link} icon={<LinkTop />}/>
                    </Box>
                    <Text color="brand" size="large">
                        {moment(post.create_date_in_s*1000).format("dddd DD.MM.YY")}
                    </Text>
                </CardFooter>
            </Card>
        )
    }
}

export default PostCard;