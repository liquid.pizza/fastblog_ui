import React, { Component } from 'react';
import config from "../conf/config.json";
import PostCard from '../postcard/PostCard';
import {
    Grid
} from "grommet";

class GetOnlinePosts extends Component {
    constructor(props){
        super(props);
        this.state = {
            error : null,
            isLoaded : false,
            posts : []
        };
    }

    componentDidMount() {

        console.log("Load data")

        fetch(config.backend_url)
        .then( response => response.json())
        .then(
            // handle the result
            (result) => {
                this.setState({
                    isLoaded : true,
                    posts : result
                });
                console.log("Loaded data")
            },

            // Handle error
            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                });
                console.log("Error loading", error)
            },
        )
    }

    render() {
        const {error, isLoaded, posts} = this.state;

        if(error){
            return <div>Error in loading</div>
        }else if (!isLoaded) {
            return <div>Loading ...</div>
        }else{
            return(
                <Grid columns="large" gap="large" pad={{ bottom: "large" }}>
                {
                    posts.map(post => (
                        <PostCard post={post}/>
                    ))
                }
                </Grid>
            );
        }
    }
  }

export default GetOnlinePosts;
